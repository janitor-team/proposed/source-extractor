Source: source-extractor
Section: science
Priority: optional
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper-compat (= 12),
               libatlas-base-dev,
               libfftw3-dev
Standards-Version: 4.4.1
Homepage: https://www.astromatic.net/software/sextractor
Vcs-Git: https://salsa.debian.org/debian-astro-team/source-extractor.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/source-extractor

Package: source-extractor
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: sextractor (<< 2.25.0+ds-1~)
Replaces: sextractor (<< 2.25.0+ds-1~)
Description: Source extractor for astronomical images
 Find sources, such as stars and galaxies, in astronomical images.
 The input data files are in FITS file format, and are analyzed to
 compute the locations of sources, with the ability to distinguish
 between galaxies and stars using a neural-network technique.

Package: sextractor
Architecture: all
Depends: source-extractor, ${misc:Depends}
Section: oldlibs
Description: Dummy transitional package for name change
 The original package name is seen as no longer appropriate by the
 astrophysics community, triggering a name change. This package
 ensures a smooth transition to the new package name. The transitional
 package can be safely removed.
