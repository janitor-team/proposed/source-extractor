Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sextractor
Upstream-Author: Emmanuel Bertin <bertin@iap.fr>
Source: https://www.astromatic.net/software/sextractor
Files-Excluded: debian/

Files: *
Copyright: 1993-2018 Emmanuel Bertin <bertin@iap.fr> IAP-CNRS/Univ,P.&M.Curie
 1993-2017 IAP/CNRS/UPMC,
 1995-1999 Mark Calabretta <mcalabre@atnf.csiro.au>,
 2004-2005  Manolis Lourakis (lourakis at ics forth gr),
 2015-2019 Ole Streicher <olebole@debian.org>
License: GPL-3.0+
 SExtractor is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 SExtractor is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".
Comment: The files in src/wcs/ and src/levmar were originally released under
 GPL-2.0+ and LGPL-2.0+, but changed and relicensed under GPLv3+ by the
 upstream author Emmanuel Bertin.
 .
 This package is based on a public-domain debianization of version 2.4.4 by
 Justin Pryzby <justinpryzby@users.sf.net> from 2004-2005, with contributions
 by Florian Ernst <florian@uni-hd.de>.

Files: autogen.sh
Copyright: 2005-2009 United States Government as represented by
 the U.S. Army Research Laboratory.
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 .
 3. The name of the author may not be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
